package cpsc441;

import cpsc441.doNOTmodify.*;
import cpsc441.solution.Router;
import cpsc441.support.CheckDone;
import cpsc441.support.FakeLog;
import cpsc441.support.FakeLogFactory;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import static org.junit.Assert.*;
import org.junit.Test;


public class Simulation {

    public static String getTableMessageFor(int id, FakeLogFactory factory) {
        String name = "router"+id+".log";
        FakeLog log = factory.getLog(name);
        if(log == null) return "";
        List<String> msgs = log.getMessages();
        if(msgs.size() == 0) {
            StringBuilder buf = new StringBuilder();
            for(String s : factory.logs.keySet()) {
                buf.append(s).append(", ");
            }
            return String.format("Log [ %s ] Empty ; [ %s ] available", name, buf.substring(0, buf.length()-2));
        }
        return msgs.get(msgs.size()-1);
    }

    public static void simulate(final Topology topology, final ILogFactory factory, final int runTime, final int mailboxSize) throws Exception {

        final Runnable [] routers = new Runnable[topology.getNumRouters()];
        final CyclicBarrier forStopCheck = new CyclicBarrier(topology.getNumRouters()+1);

        final BlockingQueue<DVRInfo> nemBox = new LinkedBlockingQueue<DVRInfo>(mailboxSize);
        final BlockingQueue<DVRInfo> [] mailboxes = new BlockingQueue[topology.getNumRouters()];
        for(int i=0; i<topology.getNumRouters(); i++) {
            mailboxes[i] = new LinkedBlockingQueue<DVRInfo>(mailboxSize);
        }

        for(int i=0; i<topology.getNumRouters(); i++){
            final IUDPSocket sock = new FakeUDPSocket(i, nemBox, mailboxes);
            routers[i] = new CheckDone(new Router(i, sock, factory), forStopCheck);
        }

        final IUDPSocket nemSock = new FakeUDPSocket(HelperUtils.getNemId(), nemBox, mailboxes);
        final IUDPSocket lNemSock = nemSock;
        final NEM nem = new NEM(topology, lNemSock);

        final ExecutorService exec = Executors.newFixedThreadPool(topology.getNumRouters() + 1);
        exec.submit(nem);
        for(int i=0; i<topology.getNumRouters(); i++) {
            exec.submit(routers[i]);
        }

        Thread.sleep(runTime);
        nem.shutDown(forStopCheck, topology.getNumRouters());
        forStopCheck.await();
        //forStopCheck.await(runTime, TimeUnit.MILLISECONDS);
        exec.shutdownNow();
    }
}


class NEM implements Runnable {

    final Topology topology;
    final IUDPSocket sock;
    boolean flag;

    public NEM(Topology topology, IUDPSocket sock) {
        this.topology = topology;
        this.sock = sock;
        this.flag = true;
    }

    public void run () {
        try{
            sock.setSoTimeout((int)2e3);
            while(flag) {
                try {
                    DVRInfo info = sock.receive();
                    if (info != null && info.type == DVRInfo.PKT_HELLO) {
                        DVRInfo route = new DVRInfo(HelperUtils.getNemId(), info.sourceid, 0, DVRInfo.PKT_ROUTE);
                        int[] weights = topology.getWeightsForRouter(info.sourceid);
                        route.mincost = Arrays.copyOf(weights, weights.length);
                        System.out.printf("NEM: got [ %s ], sending [ %s ]\n",info.toString(), route.toString());
                        System.out.flush();
                        sock.send(route);
                    }
                } catch (SocketTimeoutException e) {
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch(SocketException e) {
            e.printStackTrace();
        } finally {
            System.out.printf("================NEM FINISHING================\n");
        }
    }

    public void shutDown(CyclicBarrier cb, int numRouters) {
        try {
            final DVRInfo quit = new DVRInfo(HelperUtils.getNemId(), 0, 0, DVRInfo.PKT_QUIT);
            while(cb.getNumberWaiting() < numRouters) {
                for(int i=0; i<topology.getNumRouters(); i++) {
                    final DVRInfo cpy = new DVRInfo(quit);
                    cpy.destid = i;
                    sock.send(cpy);
                }
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        } finally {
            flag = false;
        }

    }
}

class FakeUDPSocket implements IUDPSocket {

    int timeout = HelperUtils.getArqTimer();

    int id;
    BlockingQueue<DVRInfo> nem;
    BlockingQueue<DVRInfo> [] mailboxes;

    public FakeUDPSocket(int id, BlockingQueue<DVRInfo> nem, BlockingQueue<DVRInfo> [] mailboxes) {
        this.id = id;
        this.nem = nem;
        this.mailboxes = mailboxes;
    }

    @Override
    public void setSoTimeout(int msTimeout) throws SocketException {
        timeout = msTimeout;
    }

    @Override
    public DVRInfo receive() throws IOException {
        try {
            BlockingQueue<DVRInfo> mailbox;
            if(id == HelperUtils.getNemId()) {
                mailbox = nem;
            } else {
                mailbox = mailboxes[id];
            }
            DVRInfo ret = mailbox.poll(timeout, TimeUnit.MILLISECONDS);
            if(ret == null) {
                throw new SocketTimeoutException();
            } else {
                return ret;
            }
        }catch(InterruptedException e) {
            throw new SocketTimeoutException();
        }
   }

    @Override
    public void send(DVRInfo info) throws IOException {
        if(info.destid == HelperUtils.getNemId()){
            nem.offer(info);
        } else {
            mailboxes[info.destid].offer(info);
        }
    }

}
