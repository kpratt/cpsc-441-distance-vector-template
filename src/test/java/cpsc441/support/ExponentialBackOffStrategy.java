package cpsc441.support;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ExponentialBackOffStrategy {

    private static final Random r = new Random();
    private static final int SMALL_CAP = 8;
    private int cap = SMALL_CAP;

    public boolean await(int timeout, Condition c) throws InterruptedException{
        Calendar limit = Calendar.getInstance();
        limit.add(Calendar.MILLISECOND, timeout);

        try {
            while(limit.after(Calendar.getInstance()) && !c.isMet()) {
                sleep();
            }
        } finally {
            reset();
        }
        return c.isMet();
    }

    public void sleep() throws InterruptedException {
        cap = cap << 1; // Bit shift (ie. cap * 2)
        int sleep = r.nextInt(cap);
        Thread.sleep(sleep);
    }

    public void reset() {
        cap = SMALL_CAP;
    }
}
