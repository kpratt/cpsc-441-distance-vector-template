package cpsc441.support;

public interface Condition {
    public boolean isMet();
}
