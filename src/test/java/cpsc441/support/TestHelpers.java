package cpsc441.support;

import cpsc441.doNOTmodify.DVRInfo;
import cpsc441.doNOTmodify.HelperUtils;

import java.net.SocketTimeoutException;
import java.util.Arrays;

public class TestHelpers {

    public static final int NEM_ID = HelperUtils.getNemId();
    public static final int INF = DVRInfo.COST_INFTY;


    public static Either<SocketTimeoutException, DVRInfo> e(DVRInfo x) {
        return Either.<SocketTimeoutException, DVRInfo>Right(x);
    }

    public static Either<SocketTimeoutException, DVRInfo> e(SocketTimeoutException x) {
        return Either.<SocketTimeoutException, DVRInfo>Left(x);
    }

    public static DVRInfo createDVRInfo(int senderID, int receiverID, int type, int [] minCost) {
        DVRInfo ret = new DVRInfo(senderID, receiverID, 0, type);
        ret.mincost = Arrays.copyOf(minCost, minCost.length);
        return ret;
    }


}
