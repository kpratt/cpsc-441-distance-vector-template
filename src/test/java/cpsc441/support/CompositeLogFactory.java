package cpsc441.support;

import cpsc441.doNOTmodify.ILogFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class CompositeLogFactory implements ILogFactory{

    final List<ILogFactory> facs;

    public CompositeLogFactory(List<ILogFactory> facs) {
        this.facs = facs;
    }

    @Override
    public PrintStream newLogFile(File file) throws FileNotFoundException, IOException {
        List<PrintStream> streams = new ArrayList<PrintStream>();
        for(ILogFactory fac : facs) {
            streams.add(fac.newLogFile(file));
        }
        return new CompositePrintStream(streams);
    }
}
