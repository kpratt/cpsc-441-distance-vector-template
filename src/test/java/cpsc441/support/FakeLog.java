package cpsc441.support;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class FakeLog extends PrintNothingStream {

    List<String> messages = new ArrayList<String>();

    public FakeLog(File file) {
    }

    @Override
    public void println(String x) {
        messages.add(x);
    }

    public List<String> getMessages(){
        return Collections.unmodifiableList(messages);
    }
    @Override
    public PrintStream printf(String format, Object... args) {
        String str = String.format(format, args);
        if(str.charAt(str.length()-1) == '\n') {
            str = str.substring(0, str.length() - 1);
        }
        messages.add(str);
        return this;
    }

    @Override
    public PrintStream printf(Locale l, String format, Object... args) {
        messages.add(String.format(l, format, args));
        return this;
    }

    @Override
    public void close () {}

    @Override
    public void flush(){}

}
