package cpsc441.support;

public abstract class Either<S, T> {
    public static <S, T> Either<S, T> Right(T t) {
        return new Right<S,T>(t);
    }

    public static <S, T> Either<S, T> Left(S s) {
        return new Left<S,T>(s);
    }

    public boolean isLeft() {
        return false;
    }

    public boolean isRight() {
        return false;
    }

    public S left() {
        throw new NullPointerException();
    }

    public T right() {
        throw new NullPointerException();
    }
}

class Left<S,T> extends Either<S,T> {
    final S thing;
    public Left(S s){
        this.thing = s;
    }

    public boolean isLeft() {
        return true;
    }

    public S left() {
        return this.thing;
    }

}

class Right<S,T> extends Either<S,T> {
    final T thing;

    public Right(T t){
        this.thing = t;
    }

    public boolean isRight() {
        return true;
    }

    public T right() {
        return this.thing;
    }
}