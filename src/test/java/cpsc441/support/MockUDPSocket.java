package cpsc441.support;

import cpsc441.doNOTmodify.DVRInfo;
import cpsc441.doNOTmodify.IUDPSocket;
import org.junit.Assert;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

public class MockUDPSocket implements IUDPSocket {

    final List<DVRInfo> expectedSend;
    final List<Either<SocketTimeoutException, DVRInfo>> receivable;
    boolean timeoutSet;
    int msTimeout = 0;
    ConcurrentLinkedQueue<DVRInfo> extraPacketsSent;
    final boolean ignoreExtra;
    final boolean timeoutOnRecieveBufferEmpty;

    public MockUDPSocket(List<Either<SocketTimeoutException, DVRInfo>> receivable, List<DVRInfo> expectedSend, boolean ignoreExtra, boolean timeoutOnRecieveBufferEmpty) {
        this.expectedSend = expectedSend;
        this.receivable = receivable;
        this.timeoutSet = false;
        this.ignoreExtra = ignoreExtra;
        this.timeoutOnRecieveBufferEmpty = timeoutOnRecieveBufferEmpty;
        this.extraPacketsSent = new ConcurrentLinkedQueue<DVRInfo>();
    }

    @Override
    public void setSoTimeout(int msTimeout) throws SocketException {
        if(msTimeout > 0) {
            timeoutSet = true;
            this.msTimeout = msTimeout;
        }
    }

    @Override
    public DVRInfo receive() throws IOException {
        if(receivable.isEmpty() && timeoutOnRecieveBufferEmpty) {
            ExponentialBackOffStrategy backoff = new ExponentialBackOffStrategy();
            try {
                backoff.await(msTimeout, new Condition() {
                    @Override
                    public boolean isMet() {
                        return false;
                    }
                });
            } catch (InterruptedException e) {
            } finally {
                throw new SocketTimeoutException();
            }
        } else if(receivable.isEmpty()) {
            throw new RuntimeException("Receive after Quit?");
        } else {
            Either<SocketTimeoutException, DVRInfo> either = receivable.remove(0);
            if(either.isLeft()) {
                throw either.left();
            } else {
                return either.right();
            }
        }
    }

    @Override
    public void send(DVRInfo info) throws IOException {
        if(expectedSend.isEmpty()) {
            extraPacketsSent.add(info);
        } else if(!expectedSend.remove(info)) {
            extraPacketsSent.add(info);
        }
    }

    public void assertTimeoutSet(){
        if(!timeoutSet) {
            Assert.fail("Timeout was never set!");
        }
    }

    public void assertAllPacketsSent() {
        if(!expectedSend.isEmpty()){
            StringBuilder buf = new StringBuilder();
            buf.append("All packets were not sent:\n");
            for(DVRInfo i : expectedSend) {
                buf.append("\t").append(i.toString()).append("\n");
            }
            Assert.fail(buf.toString());
        }
    }

    public Queue<DVRInfo> extraPackets(){
        return extraPacketsSent;
    }

    public void assertNoExtraPackets(){
        if(!extraPackets().isEmpty()) {
            StringBuilder buf = new StringBuilder();
            while( !extraPackets().isEmpty() ){
                buf.append("\t").append(extraPackets().remove()).append("\n");
            }
            Assert.fail("Extra packets were found\n" + buf.toString());
        }
    }

    public boolean awaitAllPacketsSent(int timeout){
        ExponentialBackOffStrategy backoff = new ExponentialBackOffStrategy();
        try {
            return backoff.await(timeout, new Condition() {
                @Override public boolean isMet() {
                    return expectedSend.isEmpty();
                }
            });
        } catch (InterruptedException e) {}
        return false;
    }
}
