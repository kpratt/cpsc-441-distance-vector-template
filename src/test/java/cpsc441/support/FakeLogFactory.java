package cpsc441.support;

import cpsc441.doNOTmodify.ILogFactory;
import java.util.concurrent.ConcurrentHashMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;
import java.util.TreeMap;

public class FakeLogFactory implements ILogFactory{

    public Map<String, FakeLog> logs = new ConcurrentHashMap<String, FakeLog>();

    public FakeLog getLog(String name) {
        FakeLog log = logs.get(name);
        if(log == null) {
            System.out.printf("Could not find log [ %s ] in:\n", name);
            for(String str : logs.keySet()) {
                System.out.printf("\t%s\n", str);
            }
        }
        return log;
    }

    @Override
    public PrintStream newLogFile(File file) throws FileNotFoundException, IOException {
        FakeLog ret = new FakeLog(file);
        logs.put(file.getName(), ret);
        return ret;
    }
}
