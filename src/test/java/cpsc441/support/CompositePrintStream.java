package cpsc441.support;

import java.io.File;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class CompositePrintStream extends PrintNothingStream {

    final List<PrintStream> streams;

    public CompositePrintStream(List<PrintStream> streams) {
        this.streams = streams;
    }

    @Override
    public void println(String x) {
        for(PrintStream s : streams) {
            s.println(x);
        }
    }

    @Override
    public PrintStream printf(String format, Object... args) {
        for(PrintStream s : streams) {
            s.printf(format, args);
        }
        return this;
    }

    @Override
    public PrintStream printf(Locale l, String format, Object... args) {
        for(PrintStream s : streams) {
            s.printf(l, format, args);
        }
        return this;
    }

    @Override
    public void close () {
        for(PrintStream s : streams) {
            s.close();
        }
    }

    @Override public void flush(){}
}
