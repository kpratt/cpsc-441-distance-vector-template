package cpsc441.support;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CheckDone implements Runnable {
    final Runnable underlying;
    final CyclicBarrier triggerDone;

    public CheckDone(Runnable underlying, CyclicBarrier triggerDone) {
        this.underlying = underlying;
        this.triggerDone = triggerDone;
    }

    public void run(){
        try {
            underlying.run();
            triggerDone.await();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
