package cpsc441.support;

import java.io.*;
import java.util.Locale;

// Reduces PrintStream to an interface
public abstract class PrintNothingStream  extends PrintStream {
    private static final OutputStream dummy = new ByteArrayOutputStream();
    public PrintNothingStream() {
        super(dummy);
    }

    @Override public void flush() {  }
    @Override public void close() { throw new MethodNotImplemented(); }
    @Override public boolean checkError() {return false;}
    @Override protected void setError() { throw new MethodNotImplemented(); }
    @Override protected void clearError() { throw new MethodNotImplemented(); }
    @Override public void write(int b) { throw new MethodNotImplemented(); }
    @Override public void write(byte[] buf, int off, int len) { throw new MethodNotImplemented(); }

    @Override public void print(boolean b){ throw new MethodNotImplemented(); }
    @Override public void print(char c){ throw new MethodNotImplemented(); }
    @Override public void print(int i){ throw new MethodNotImplemented(); }
    @Override public void print(long l){ throw new MethodNotImplemented(); }
    @Override public void print(float f) { throw new MethodNotImplemented(); }
    @Override public void print(double d) { throw new MethodNotImplemented(); }
    @Override public void print(char[] s) { throw new MethodNotImplemented(); }
    @Override public void print(String s) { throw new MethodNotImplemented(); }
    @Override public void print(Object obj) { throw new MethodNotImplemented(); }
    @Override public void println() { throw new MethodNotImplemented(); }
    @Override public void println(boolean x) { throw new MethodNotImplemented(); }
    @Override public void println(char x) { throw new MethodNotImplemented(); }
    @Override public void println(int x) { throw new MethodNotImplemented(); }
    @Override public void println(long x) { throw new MethodNotImplemented(); }
    @Override public void println(float x) { throw new MethodNotImplemented(); }
    @Override public void println(double x) { throw new MethodNotImplemented(); }
    @Override public void println(char[] x) { throw new MethodNotImplemented(); }
    @Override public void println(String x) { throw new MethodNotImplemented(); }
    @Override public void println(Object x) { throw new MethodNotImplemented(); }
    @Override public PrintStream printf(String format, Object... args) { throw new MethodNotImplemented(); }
    @Override public PrintStream printf(Locale l, String format, Object... args) { throw new MethodNotImplemented(); }
    @Override public PrintStream format(String format, Object... args) { throw new MethodNotImplemented(); }
    @Override public PrintStream format(Locale l, String format, Object... args) { throw new MethodNotImplemented(); }
    @Override public PrintStream append(CharSequence csq) { throw new MethodNotImplemented(); }
    @Override public PrintStream append(CharSequence csq, int start, int end) { throw new MethodNotImplemented(); }
    @Override public PrintStream append(char c) { throw new MethodNotImplemented(); }
    @Override public void write(byte[] b) throws IOException { throw new MethodNotImplemented(); }
}