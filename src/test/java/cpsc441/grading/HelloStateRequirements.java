package cpsc441.grading;

import cpsc441.doNOTmodify.DVRInfo;
import cpsc441.doNOTmodify.HelperUtils;
import cpsc441.solution.Router;
import cpsc441.support.Either;
import cpsc441.support.FakeLog;
import cpsc441.support.FakeLogFactory;
import cpsc441.support.MockUDPSocket;
import org.junit.Test;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static cpsc441.grading.RequirementTest.assertGrade;
import static cpsc441.support.TestHelpers.*;
import static cpsc441.grading.RequirementTest.*;

public class HelloStateRequirements {

    public static void sendsHelloMessageOnStartUp(){
        int myID = 3;
        final List<Either<SocketTimeoutException, DVRInfo>> inbound = new ArrayList<Either<SocketTimeoutException, DVRInfo>>();
        final List<DVRInfo> expectedSends = new CopyOnWriteArrayList<DVRInfo>();

        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        final MockUDPSocket mockSocket = new MockUDPSocket(inbound, expectedSends, true, true);
        final FakeLogFactory fakeLogFactory = new FakeLogFactory();

        final Router r = new Router(myID, mockSocket, fakeLogFactory);
        final ExecutorService exec = Executors.newFixedThreadPool(1);
        exec.submit(r);

        try {
            if(mockSocket.awaitAllPacketsSent(100)){
                assertGrade(10, "Hello message was sent.");
            } else {
                assertGrade(0, "Hello message was NOT sent.");
            }
        } catch (Exception e) {
            assertGrade(0, "Hello message was NOT sent.", e);
        } finally {
            exec.shutdownNow();
        }
    }

    public static void staysInHelloState(){
        int myID = 3;
        final List<Either<SocketTimeoutException, DVRInfo>> inbound = new CopyOnWriteArrayList<Either<SocketTimeoutException, DVRInfo>>();
        final List<DVRInfo> expectedSends = new CopyOnWriteArrayList<DVRInfo>();

        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        inbound.add(e(new SocketTimeoutException()));
        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        inbound.add(e(new SocketTimeoutException()));
        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        inbound.add(e(new SocketTimeoutException()));
        final MockUDPSocket mockSocket = new MockUDPSocket(inbound, expectedSends, true, true);
        final FakeLogFactory fakeLogFactory = new FakeLogFactory();

        final Router r = new Router(myID, mockSocket, fakeLogFactory);
        final ExecutorService exec = Executors.newFixedThreadPool(1);
        exec.submit(r);

        try {
            if(mockSocket.awaitAllPacketsSent(100)){
                assertGrade(10, "Hello message was successfully retransmitted on timeout.");
            } else {
                assertGrade(0, "Hello message was NOT retransmitted on timeout.");
            }
        } catch (Exception e) {
            assertGrade(0, "Hello message was NOT retransmitted on timeout.", e);
        } finally {
            exec.shutdownNow();
        }
    }
}
