package cpsc441.grading;

import cpsc441.doNOTmodify.DVRInfo;
import cpsc441.doNOTmodify.IUDPSocket;
import cpsc441.solution.Router;
import cpsc441.support.*;
import org.junit.Test;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static cpsc441.grading.RequirementTest.assertGrade;
import static cpsc441.support.TestHelpers.*;
import static cpsc441.support.TestHelpers.INF;

public class TransmissionRequirements {

    public static void setsTimeoutBeforeReceiveCalled() {

        final TimeOutCheck mockSocket = new TimeOutCheck();
        final FakeLogFactory fakeLogFactory = new FakeLogFactory();

        final Router r = new Router(3, mockSocket, fakeLogFactory);
        final ExecutorService exec = Executors.newFixedThreadPool(1);

        exec.submit(r);
        try {
            ExponentialBackOffStrategy backoff = new ExponentialBackOffStrategy();
            backoff.await(100, new Condition() {
                @Override
                public boolean isMet() {
                    return mockSocket.wasSet();
                }
            });

            if(mockSocket.wasSet()){
                assertGrade(10, "Socket was put in non-blocking mode (Timeout Set).");
            } else {
                assertGrade(0, "Socket was NOT put in non-blocking mode (Timeout Set).");
            }
        } catch (Exception e) {
            assertGrade(0, "Socket was NOT put in non-blocking mode (Timeout Set).", e);
        } finally {
            exec.shutdownNow();
        }
    }


    public static void retransmissionOfRoutingMessagesOnTimeout() {
        int myID = 1;
        final List<Either<SocketTimeoutException, DVRInfo>> inbound = new CopyOnWriteArrayList<Either<SocketTimeoutException, DVRInfo>>();
        final List<DVRInfo> expectedSends = new CopyOnWriteArrayList<DVRInfo>();

        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        inbound.add(e(createDVRInfo(NEM_ID, myID, DVRInfo.PKT_ROUTE, new int[]{INF, 0, INF, INF, 1})));

        expectedSends.add(createDVRInfo(myID, 4, DVRInfo.PKT_ROUTE, new int [] {INF, 0, INF, INF, 1}));
        inbound.add(e(new SocketTimeoutException()));

        expectedSends.add(createDVRInfo(myID, 4, DVRInfo.PKT_ROUTE, new int [] {INF, 0, INF, INF, 1}));
        inbound.add(e(new SocketTimeoutException()));

        expectedSends.add(createDVRInfo(myID, 4, DVRInfo.PKT_ROUTE, new int [] {INF, 0, INF, INF, 1}));
        inbound.add(e(new SocketTimeoutException()));

        expectedSends.add(createDVRInfo(myID, 4, DVRInfo.PKT_ROUTE, new int [] {INF, 0, INF, INF, 1}));
        inbound.add(e(new SocketTimeoutException()));

        expectedSends.add(createDVRInfo(myID, 4, DVRInfo.PKT_ROUTE, new int [] {INF, 0, INF, INF, 1}));
        inbound.add(e(new SocketTimeoutException()));


        final MockUDPSocket mockSocket = new MockUDPSocket(inbound, expectedSends, true, true);
        final FakeLogFactory fakeLogFactory = new FakeLogFactory();

        final Router r = new Router(myID, mockSocket, fakeLogFactory);
        final ExecutorService exec = Executors.newFixedThreadPool(1);
        exec.submit(r);

        try {
            if(mockSocket.awaitAllPacketsSent((int)1e4)){
                assertGrade(10, "Route message was successfully retransmitted on timeout.");
            } else {
                System.out.println("Unexpected packets during retransmit test:");
                for(DVRInfo extra : mockSocket.extraPackets()){
                    System.out.println(extra);
                }

                System.out.println("Leftover packets during retransmit test:");
                for(DVRInfo leftover : expectedSends){
                    System.out.println(leftover);
                }

                System.out.println("Log Dump");
                FakeLog log = fakeLogFactory.getLog("router1.log");
                if(log == null) {
                    assertGrade(0, "No log during retransmit test.");
                } else {
                    for(String str : log.getMessages()){
                        System.out.println(str);
                    }
                    assertGrade(0, "Route message was NOT retransmitted on timeout.");
                }

            }
        } catch (Exception e) {
            assertGrade(0, "Route message was NOT retransmitted on timeout.", e);
        } finally {
            exec.shutdownNow();
        }
    }

    public static void onlyBroadCastsToNeighbors(){
        int myID = 4;
        List<Either<SocketTimeoutException, DVRInfo>> inbound = new CopyOnWriteArrayList<Either<SocketTimeoutException, DVRInfo>>();
        List<DVRInfo> expectedSends = new CopyOnWriteArrayList<DVRInfo>();

        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        inbound.add(e(createDVRInfo(NEM_ID, myID, DVRInfo.PKT_ROUTE, new int[]{3, INF, INF, 5, 0})));

        expectedSends.add(createDVRInfo(myID, 0, DVRInfo.PKT_ROUTE, new int [] {3, INF, INF, 5, 0}));
        expectedSends.add(createDVRInfo(myID, 3, DVRInfo.PKT_ROUTE, new int[]{3, INF, INF, 5, 0}));

        inbound.add(e(createDVRInfo(NEM_ID, myID, DVRInfo.PKT_QUIT, new int [] {})));

        MockUDPSocket mockSocket = new MockUDPSocket(inbound, expectedSends, false, true);
        FakeLogFactory fakeLogFactory = new FakeLogFactory();

        final ExecutorService exec = Executors.newFixedThreadPool(1);
        try {
            Router r = new Router(myID, mockSocket, fakeLogFactory);
            exec.submit(r);

            Thread.sleep(100);
            if(!mockSocket.extraPackets().isEmpty()) {

                boolean invalidPacketSent = false;
                for(DVRInfo exp : mockSocket.extraPackets()) {
                    if(exp.destid != NEM_ID && exp.destid != 0 && exp.destid != 3) {
                        invalidPacketSent = true;
                        break;
                    }
                }
                if(invalidPacketSent) {
                    assertGrade(0, "Extra packets sent.");
                } else {
                    assertGrade(10, "All Packets where sent over valid links.");
                }
            } else {
                assertGrade(10, "All Packets where sent over valid links.");
            }
        } catch (Exception e) {
            System.out.println("========================== derp!");
            e.printStackTrace();
            assertGrade(0, "May have sent to non-neighbor.", e);
        } finally {
            exec.shutdownNow();
        }
    }
}


class TimeOutCheck implements IUDPSocket {

    boolean timeoutSet = false;

    @Override
    public void setSoTimeout(int msTimeout) throws SocketException {
        timeoutSet = true;
    }

    @Override
    public DVRInfo receive() throws IOException {
        while(true);
    }

    @Override
    public void send(DVRInfo info) throws IOException {

    }

    public boolean wasSet() {
        return timeoutSet;
    }
}