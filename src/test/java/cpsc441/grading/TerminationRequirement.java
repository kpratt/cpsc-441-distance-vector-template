package cpsc441.grading;

import cpsc441.doNOTmodify.DVRInfo;
import cpsc441.solution.Router;
import cpsc441.support.CheckDone;
import cpsc441.support.Either;
import cpsc441.support.FakeLogFactory;
import cpsc441.support.MockUDPSocket;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static cpsc441.grading.RequirementTest.assertGrade;
import static cpsc441.support.TestHelpers.*;
import static cpsc441.support.TestHelpers.INF;

public class TerminationRequirement {
    public static void terminatesOnQuitePacket(){
        int myID = 3;
        final List<Either<SocketTimeoutException, DVRInfo>> inbound = new CopyOnWriteArrayList<Either<SocketTimeoutException, DVRInfo>>();
        final List<DVRInfo> expectedSends = new CopyOnWriteArrayList<DVRInfo>();

        expectedSends.add(createDVRInfo(myID, NEM_ID, DVRInfo.PKT_HELLO, new int [] {}));
        inbound.add(e(createDVRInfo(NEM_ID, myID, DVRInfo.PKT_ROUTE, new int[]{INF, INF, INF, 0, INF})));
        inbound.add(e(createDVRInfo(NEM_ID, myID, DVRInfo.PKT_QUIT, new int [] {})));

        final MockUDPSocket mockSocket = new MockUDPSocket(inbound, expectedSends, false, false);
        final FakeLogFactory fakeLogFactory = new FakeLogFactory();

        final Router r = new Router(myID, mockSocket, fakeLogFactory);
        final ExecutorService exec = Executors.newFixedThreadPool(1);
        final CyclicBarrier cb = new CyclicBarrier(2);
        final CheckDone doneCheck = new CheckDone(r, cb);
        exec.submit(doneCheck);

        try {
            cb.await(100, TimeUnit.MILLISECONDS);
            assertGrade(10, "Terminated on QUIT.");
        } catch (TimeoutException e) {
            assertGrade(0, "Failed to terminate.");
        } catch (Exception e) {
            assertGrade(0, "Hello message was NOT sent.", e);
        } finally {
            exec.shutdownNow();
        }
    }


}
