package cpsc441.grading;

import cpsc441.doNOTmodify.ILogFactory;
import cpsc441.doNOTmodify.LogFactory;
import cpsc441.doNOTmodify.Topology;
import cpsc441.support.CompositeLogFactory;
import cpsc441.support.FakeLogFactory;
import static cpsc441.Simulation.*;
import static cpsc441.grading.RequirementTest.*;
import static cpsc441.support.TestHelpers.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ConvergenceRequirements {

    public static void convergesToCorrectSolutionForSample() {

        try {
            Topology topology = Topology.create(new int[][]{
                    new int[]{0, 1, 999, 999, 7}
                    , new int[]{1, 0, 3, 2, 11}
                    , new int[]{999, 3, 0, 2, 999}
                    , new int[]{999, 2, 2, 0, 1}
                    , new int[]{7, 11, 999, 1, 0}
            });
            //the 3rd param to simulate has a negative corrolation with the packet "drop" rate.
            //once you have a working solution
            //try playing with how this effects the minimum time to converge
            List<ILogFactory> ilfs = new ArrayList<ILogFactory>();
            FakeLogFactory factory = new FakeLogFactory();
            ilfs.add(factory);
            ilfs.add(new LogFactory());
            ILogFactory comp = new CompositeLogFactory(ilfs);

            simulate(topology, comp, (int)2e4, 10);

            String [] expectations = new String [] {
                  "[0] table [0,1,4,3,4] via [0,1,1,1,1]"
                , "[1] table [1,0,3,2,3] via [0,1,2,3,3]"
                , "[2] table [4,3,0,2,3] via [1,1,2,3,3]"
                , "[3] table [3,2,2,0,1] via [1,1,2,3,4]"
                , "[4] table [4,3,3,1,0] via [3,3,3,3,4]" };

            boolean pass = true;
            for(int i=0; i<5; i++) {
                String logged = getTableMessageFor(i, factory);
                if(!expectations[i].equals(logged)) {
                    pass = false;
                    System.out.printf("[ %d ]\n", i);
                    System.out.println("actual:   " + logged);
                    System.out.println("expected: " + expectations[i]);
                }
            }

            if(!pass){
                assertGrade(0, "Simulation did NOT converge for sample topology.");
            } else {
                assertGrade(5, "Simulation converged for sample topology.");
            }
        } catch (Exception e) {
            assertGrade(0, "Simulation timed out.", e);
        }
    }

    public static void convergesToCorrectSolutionForLargeTopology() {

        try {
            Topology topology = Topology.create( new int [][] {
                  new int [] {0,    INF,    INF,    12,     1,      INF,    7,      INF,    INF,    2}
                , new int [] {INF,  0,      3,      INF,    INF,    9,      INF,    INF,    INF,    INF}
                , new int [] {INF,  INF,    0,      INF,    INF,    7,      INF,    INF,    INF,    INF}
                , new int [] {12,   INF,    INF,    0,      INF,    INF,    INF,    4,      5,      INF}
                , new int [] {1,    INF,    INF,    INF,    0,      INF,    6,      INF,    INF,    INF}
                , new int [] {INF,  9,      7,      INF,    INF,    0,      INF,    INF,    3,      INF}
                , new int [] {7,    INF,    INF,    INF,    6,      INF,    0,      INF,    1,      INF}
                , new int [] {INF,  INF,    INF,    4,      INF,    INF,    INF,    0,      INF,    INF}
                , new int [] {INF,  INF,    INF,    5,      INF,    3,      1,      INF,    0,      1}
                , new int [] {2,    INF,    INF,    INF,    INF,    INF,    INF,    INF,    1,      0}
            });

            //the 3rd param to simulate has a negative corrolation with the packet "drop" rate.
            //once you have a working solution
            //try playing with how this effects the minimum time to converge

            List<ILogFactory> ilfs = new ArrayList<ILogFactory>();
            FakeLogFactory factory = new FakeLogFactory();
            ilfs.add(factory);
            ilfs.add(new LogFactory());
            ILogFactory comp = new CompositeLogFactory(ilfs);

            simulate(topology, comp, (int)7.5e4, 10);

            String [] expectations = new String [] {
                  "[0] table [0,15,13,8,1,6,4,12,3,2] via [0,9,9,9,4,9,9,9,9,9]"
                , "[1] table [15,0,3,17,16,9,13,21,12,13] via [5,1,2,5,5,5,5,5,5,5]"
                , "[2] table [13,16,0,15,14,7,11,19,10,11] via [5,5,2,5,5,5,5,5,5,5]"
                , "[3] table [8,17,15,0,9,8,6,4,5,6] via [8,8,8,3,8,8,8,7,8,8]"
                , "[4] table [1,16,14,9,0,7,5,13,4,3] via [0,0,0,0,4,0,0,0,0,0]"
                , "[5] table [6,9,7,8,7,0,4,12,3,4] via [8,1,2,8,8,5,8,8,8,8]"
                , "[6] table [4,13,11,6,5,4,0,10,1,2] via [8,8,8,8,8,8,6,8,8,8]"
                , "[7] table [12,21,19,4,13,12,10,0,9,10] via [3,3,3,3,3,3,3,7,3,3]"
                , "[8] table [3,12,10,5,4,3,1,9,0,1] via [9,5,5,3,9,5,6,3,8,9]"
                , "[9] table [2,13,11,6,3,4,2,10,1,0] via [0,8,8,8,0,8,8,8,8,9]"
            };

            boolean pass = true;
            for(int i=0; i<10; i++) {
                String logged = getTableMessageFor(i, factory);
                if(!expectations[i].equals(logged)) {
                    pass = false;
                    System.out.printf("[ %d ]\n", i);
                    System.out.println("actual:   " + logged);
                    System.out.println("expected: " + expectations[i]);
                }
            }

            if(!pass){
                assertGrade(0, "Simulation did NOT converge for large topology.");
            } else {
                assertGrade(15, "Simulation converged for large topology.");
            }
        } catch (Exception e) {
            assertGrade(0, "Simulation timed out.", e);
        }
    }
}
