package cpsc441.grading;

import cpsc441.doNOTmodify.LogFactory;
import org.junit.Test;

import java.io.*;
import java.security.Permission;

public class RequirementTest {

    private static PrintStream report;
    private static int score = 0;

    static {
        try {
            File file = new File("./Grading_Report.txt");
            LogFactory fac = new LogFactory();
            PrintStream ps = fac.newLogFile(file);
            report = ps;
        } catch (FileNotFoundException e) {
            report = null;
        } catch (IOException e) {
            report = null;
        }
    }

    public static void assertGrade(int points, String criteria) {
        report.printf("%d : %s\n", points, criteria);
        score += points;
    }

    public static void assertGrade(int points, String criteria, Exception e) {
        report.printf("%d : %s\n", points, criteria);
        e.printStackTrace();
        score += points;
    }

    public static void report (){
        report.printf("Total (automated) Score: [ %d / 95 ] \n", score);
        report.printf("Total (manual) Score:    [    /  5 ] \n");
    }

    private static class ExitTrappedException extends SecurityException { }

    private static void forbidSystemExitCall() {
        final SecurityManager securityManager = new SecurityManager() {
            public void checkPermission( Permission permission ) {
                if( "exitVM".equals( permission.getName() ) ) {
                    throw new ExitTrappedException() ;
                }
            }
        } ;
        System.setSecurityManager( securityManager ) ;
    }

    @Test
    public void gradeAssignment() {

        forbidSystemExitCall();

        //compiles on csc/csd/cse/csl Linux boxes
        try { CompilationRequirement.doesCompile(); } catch(Exception e ) {}

        //Follows correct naming / running convention
        try { CompilationRequirement.testsCompile(); } catch(Exception e ) {}

        //Sends Hello Message
        try { HelloStateRequirements.sendsHelloMessageOnStartUp(); } catch(Exception e ) {}

        //Stays in Hello State until response is received
        try { HelloStateRequirements.staysInHelloState(); } catch(Exception e ) {}

        //Was non-blocking mode used correctly?
        try { TransmissionRequirements.setsTimeoutBeforeReceiveCalled();  } catch(Exception e ) {}

        //Transmission/retransmission of routing messages after time-out
        try { TransmissionRequirements.retransmissionOfRoutingMessagesOnTimeout();  } catch(Exception e ) {}

        //Transmits only to neighbors
        try { TransmissionRequirements.onlyBroadCastsToNeighbors();   } catch(Exception e ) {}


        //Correctly Terminates on completion
        try { TerminationRequirement.terminatesOnQuitePacket(); } catch(Exception e ) {}

        try { ConvergenceRequirements.convergesToCorrectSolutionForSample(); } catch(Exception e ) {}
        try { ConvergenceRequirements.convergesToCorrectSolutionForLargeTopology(); } catch(Exception e ) {}

        report();
    }
}
