package cpsc441.grading;


import static cpsc441.grading.RequirementTest.*;

public class CompilationRequirement {

    public static void doesCompile(){
        //obviously if this executes you get all the points
        assertGrade(10, "Compiled Successfully.");
    }

    public static void testsCompile(){
        //This checks Router, however Main must be visually inspected
        assertGrade(5, "Didn't modify the code :).");
    }


}
