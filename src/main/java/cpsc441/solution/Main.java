package cpsc441.solution;

import cpsc441.doNOTmodify.ILogFactory;
import cpsc441.doNOTmodify.IUDPSocket;
import cpsc441.doNOTmodify.LogFactory;
import cpsc441.doNOTmodify.UDPSocket;

import java.net.DatagramSocket;
import java.net.InetAddress;

public class Main {

    public static void main(String [] args) {
        //These two values need to be initialized from args
        // before the algorithm (which should be implemented
        // in the Router class) can work...
        int id = -1;
        int nemPort = -1;
        InetAddress nemHost = null;
        DatagramSocket s = null;

        //DO NOT modify this code.
        IUDPSocket sock = new UDPSocket(s, nemHost, nemPort);
        ILogFactory logFactory = new LogFactory();
        Router r = new Router(id, sock, logFactory);
        r.run();
        //////////////////////////////////////////

    }


}
